use std::env;
use std::io::{Write, Read};
use std::fs::File;
use std::fmt::{self, Formatter, Display};
use std::collections::HashMap; // HashMap<String, String>
use async_std::task;
use reqwest;
use serde::{Deserialize, Serialize};
use serde_json;

use std::time::Duration;
use crossterm::event::{poll, read, Event};


#[derive(Serialize, Deserialize)]
struct Data {
    sample_url: String,
    md5: String,
    id: u32,
}

#[derive(Debug, Deserialize)]
struct MD5 {
    md5: Vec<String>,
}

impl Display for Data {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "ID = {} | MD5 = {}\n{}", self.id, self.md5, self.sample_url)
    }
}

async fn get_request(url: &str) -> Result<Vec<Data>, Box<dyn std::error::Error>> {
    let resp: Vec<Data> = reqwest::get(url)
        .await?
        .json::<Vec<Data>>()
        .await?;
    Ok(resp)
}

async fn post_image(url: &str) -> Result<reqwest::Response, Box<dyn std::error::Error>> {
    // {
    //    "embeds" : [
    //        {
    //            "image" : {
    //                "url" : "url"
    //            }
    //        }
    //    ]
    //}
    let mut image_url = HashMap::new();
    image_url.insert("url", url);

    let mut image_dict = HashMap::new();
    image_dict.insert("image", image_url);

    let embeds = [image_dict];

    let mut map = HashMap::new();
    map.insert("embeds", embeds);

    let token = env::var("WEBHOOK_TOKEN").unwrap();
    //let token = "7x42UBeqT5Q-O1Lw9XoolwKUF57TuCHN2yiems1Za7qDSVrDCOFVt2tv4UJLKzAyt62R";
    let url : &str = &format!("https://canary.discordapp.com/api/webhooks/662692408542363687/{}", token).to_owned()[..];
    
    let client = reqwest::Client::new();
    let res = client.post(url)
        .json(&map)
        .send()
        .await?;

    Ok(res)
}

fn read_db() -> Vec<String> {
    let mut file = File::open("database.json").unwrap();
    let mut data = String::new();
    file.read_to_string(&mut data).unwrap();

    let json: MD5 = serde_json::from_str(&data.to_owned()[..])
        .expect("JSON was not well-formatted");

    json.md5
}

fn write_db(vec: &Vec<&String>) -> std::io::Result<()> {
    let mut md5 = HashMap::new();
    md5.insert("md5", vec);

    let mut file = File::create("database.json")?;
    let data = serde_json::to_string(&md5).unwrap();
    write!(&mut file, "{}", data)?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), reqwest::Error> {
    let mut key: crossterm::event::KeyCode;

    let tags = "touhou%20rating:e%20-loli";

    //let url: &str = &format!("https://e621.net/post/index.json?limit=320&tags={}", tags).to_owned()[..];
    let url: &str = &format!("https://yande.re/post/index.json?limit=100&tags={}", tags).to_owned()[..];
    println!("{}", url);

    loop {
        let mut md5s = Vec::new();
        let resp = get_request(&url)
            .await
            .expect("An error occurred getting the images");

        let md5s_sent = read_db();
        for item in &resp {
            //println!("{}", item);
            md5s.push(&item.md5);

            let mut in_list = false;
            for i in &md5s_sent {
                if i == &item.md5 {
                    in_list = true;
                }
            }

            if !in_list {
                post_image(&item.sample_url)
                    .await
                    .unwrap();

                task::sleep(std::time::Duration::from_secs(2))
                    .await;
            }
        }
        write_db(&md5s).unwrap();

        if poll(Duration::from_secs(120)).unwrap() {
            match read().unwrap() {
                Event::Key(event) => key = event.code,
                _ => continue,
            }

            if key == crossterm::event::KeyCode::Char('q') {
                break;
            }

        }
        println!("Iteration happened.");
    }
    Ok(())
}
